import { EventTrack } from 'amis/lib/types'

export type TrackerEventArgs = { tracker: EventTrack; eventProps: any }
